const loginModalDel = (el, back, bol, email) => {
  setTimeout(() => {
    document.querySelector(el).remove();
    document.body.classList.remove(back);
  }, 200);

  if (bol) {
    document.querySelector(".navbar__login").textContent = "Выйти из " + email;
  } else {
    return;
  }
};

export default loginModalDel;
