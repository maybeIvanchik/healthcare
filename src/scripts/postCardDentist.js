const postCardDentist = async (
  token,
  name,
  doctor,
  visitTarget,
  urgency,
  desc,
  lastVisit,
  requestType,
  cardNumber
) => {
  const post = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardNumber}`, {
    method: requestType,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: name,
      doctor: doctor,
      visitTarget: visitTarget,
      urgency: urgency,
      desc: desc,
      moreInfo: {
        lastVisit: lastVisit,
      },
    }),
  });
  if (post.ok) {
    return post.json();
  } else {
    return "NEt";
  }
};

export default postCardDentist;
