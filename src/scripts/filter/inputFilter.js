const getGilteredInputValue = (input) => {
  const cardsTitles = document.querySelectorAll(".card-title");
  const cardsDesc = document.querySelectorAll(".card-atribute_desc");

  cardsDesc.forEach((elem, index) => {
    const elContainer = elem.parentElement;
    if (!elContainer.parentElement.hidden === true) {
      const elemText = elem.textContent.split(": ")[1];
      if (!elemText.startsWith(input.value)) {
        if (!cardsTitles[index].textContent.startsWith(input.value)) {
          elContainer.parentElement.hidden = true;
        }
      }
    }
  });
};

export default getGilteredInputValue;
