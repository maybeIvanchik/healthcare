const getFilteredPriority = (select) => {
  let i;
  const priority = document.querySelectorAll(".card-atribute_priority");
  for (const char of select.children) {
    if (select.value === char.value) {
      i = char.textContent.toLowerCase();
    }
  }

  priority.forEach((el) => {
    const elContainer = el.parentElement;
    if (!el.textContent.includes(i)) {
      elContainer.parentElement.hidden = true;
      console.log(el.textContent + " -- " + i);
    }
  });
};

export default getFilteredPriority;
