import Visit from "./visit.js";

class VisitDentist extends Visit {
   constructor(cardID, name, doctor, vistTarget, urgency, desc, lastVisit) {
      super(cardID, name, doctor, vistTarget, urgency, desc)
      this.lastVisit = lastVisit
   }

   createFullCard() {
      const card = this.createCard()

      card.querySelector(".show-more-args").innerHTML = `<p class="card-atribute"><span>Последний визит: </span>${this.lastVisit}</p>`


      return card
   }
}

export default VisitDentist;