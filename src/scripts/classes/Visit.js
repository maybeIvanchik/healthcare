import cardChange from "../cardChange.js";

class Visit {
  constructor(cardID, name, doctor, vistTarget, urgency, desc) {
    this.cardID = cardID;
    this.name = name;
    this.doctor = doctor;
    this.vistTarget = vistTarget;
    this.urgency = urgency;
    this.desc = desc;
  }

  changeBtn() {
    const token = localStorage.getItem("token");
    const btn = document.createElement("div");
    btn.classList.add("card__change-btn", "card-btn");
    btn.innerHTML = `<svg class="change-btn-svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    width="20px" height="20px" viewBox="0 0 528.899 528.899" style="enable-background:new 0 0 528.899 528.899;"
    xml:space="preserve">
   <path  d="M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,361.465L328.883,89.125z M518.113,63.177l-47.981-47.981
     c-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611
     C532.495,100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069
     L27.473,390.597L0.3,512.69z"/>`;
    btn.addEventListener("click", (e) => {
      cardChange(
        ".main-page",
        this.cardID,
        this.name,
        this.vistTarget,
        this.desc,
        this.urgency,
        this.doctor,
        this.pressure,
        this.bms,
        this.pastIllnesses,
        this.age,
        this.lastVisit
      );
    });
    return btn;
  }

  deleteBtn() {
    const token = localStorage.getItem("token");
    const btn = document.createElement("div");
    btn.classList.add("card__delete-btn", "card-btn");
    btn.addEventListener("click", (el) => {
      fetch(`https://ajax.test-danit.com/api/v2/cards/${this.cardID}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then((res) => {
        el.target.parentElement.remove();
      });
    });

    return btn;
  }

  createCard() {
    const card = document.createElement("div");
    card.classList.add("card");
    card.insertAdjacentElement("afterbegin", this.deleteBtn());
    card.insertAdjacentElement("afterbegin", this.changeBtn());

    card.insertAdjacentHTML(
      "beforeend",
      `
      <div class="card-atribute-wrraper">
        <h4 class="card-title">${this.vistTarget}</h4>
        <p class="card-atribute"><span>ФИО:</span> ${this.name}</p>
        <p class="card-atribute"><span>Врач:</span> ${this.doctor}</p>
        <p class="card-atribute card-atribute_desc"><span>Краткое описание визита:</span> ${this.desc}</p>
        <p class="card-atribute card-atribute_priority"><span>Срочность:</span> ${this.urgency}</p> 
      </div>     
      `
    );

    const showMoreBtn = document.createElement("p");
    const showMoreWrraper = document.createElement("div");
    const showMoreAtributes = document.createElement("div");
    showMoreWrraper.classList.add("show-more-wrraper");
    showMoreBtn.classList.add("show-more-btn");
    showMoreAtributes.classList.add("show-more-args");

    showMoreBtn.innerText = "Показать больше";

    showMoreBtn.addEventListener("click", (e) => {
      e.target.classList.toggle("show-more-btn--active");
      e.target.previousSibling.classList.toggle("show-more-args-active");
      if (e.target.innerText == "Показать больше") {
        e.target.innerText = "Показать меньше";
      } else {
        e.target.innerText = "Показать больше";
      }
    });

    showMoreWrraper.insertAdjacentElement("beforeend", showMoreAtributes);
    showMoreWrraper.insertAdjacentElement("beforeend", showMoreBtn);
    card.insertAdjacentElement("beforeend", showMoreWrraper);

    return card;
  }
}

export default Visit;
