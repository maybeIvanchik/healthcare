import postCardCardiolist from "./postCardCardiolist.js";
import postCardDentist from "./postCardDentist.js";
import postCardTherapist from "./postCardTherapist.js";
import getVisitInfo from "./api/getVisitInfo.js";

class cardCreate {
  constructor(
    fullname,
    target,
    desc,
    priority,
    pressure,
    weightIndex,
    diseases,
    age,
    lastVisist
  ) {
    this.fullname = fullname;
    this.target = target;
    this.desc = desc;
    this.priority = priority;
    this.pressure = pressure;
    this.weightIndex = weightIndex;
    this.diseases = diseases;
    this.age = age;
    this.lastVisist = lastVisist;
  }

  render(selector) {
    document.querySelector(".body-page").classList.add("blur");
    document.querySelector(selector).insertAdjacentHTML(
      "beforeend",
      `<section class="cards-modal">
    <h2 class="cards-modal__title">Создать визит</h2>
    <form action="" method="post" class="cards-modal__form">
      <div class="cards-modal__content">
        <div class="cards-modal__section">
          <label for="fullname" class="cards-modal__fullname_label"
            >ФИО:</label
          >
          <input
            required
            type="text"
            class="cards-modal__fullname"
            id="fullname"
          />
          <label for="target" class="cards-modal__target_label"
            >Цель визита:</label
          >
          <input
            required
            type="text"
            class="cards-modal__target"
            id="target"
          />
          <label for="desc" class="cards-modal__desc_label"
            >Краткое описание визита:</label
          >
          <input required type="text" class="cards-modal__desc" id="desc" />
          <p >Приоритетность:</p>
          <select
            name="priority"
            id="priority"
            class="cards-modal__priority"
          >
            <option value="ordinary">Обычная</option>
            <option value="priority">Приоритетная</option>
            <option value="urgent">Неотложная</option>
          </select>
          <select name="doctors" id="priority" class="cards-modal__doctors">
            <option value="cardiologist">Кардиолог</option>
            <option value="dentist">Стоматолог</option>
            <option value="therapist">Терапевт</option>
          </select>
        </div>
        <div class="cards-modal__section">
          <label for="pressure" class="cards-modal__pressure_label"
            >Обычное давление:</label
          >
          <input
            required
            type="text"
            class="cards-modal__pressure"
            id="pressure"
          />
          <label for="weight-index" class="cards-modal__weight-index_label"
            >Индекс массы тела:</label
          >
          <input
            required
            type="text"
            class="cards-modal__weight-index"
            id="weight-index"
          />
          <label for="diseases" class="cards-modal__diseases_label"
            >Перенесенные заболевания сердечно-сосудистой системы:</label
          >
          <input
            required
            type="text"
            class="cards-modal__diseases"
            id="diseases"
          />
          <label for="age" class="cards-modal__age_label">Возвраст:</label>
          <input required type="text" class="cards-modal__age" id="age" />
        </div>
      </div>
      <button class="cards-modal__btn cards-modal__btn_cancel">
        Отменить
      </button>
      <button class="cards-modal__btn cards-modal__btn_submit" type="submit">
        Создать
      </button>
    </form>
  </section>`
    );

    document
      .querySelector(".cards-modal__doctors")
      .addEventListener("change", function () {
        document.querySelectorAll(".cards-modal__section")[1].innerHTML = "";

        if (this.value === "cardiologist") {
          document
            .querySelectorAll(".cards-modal__section")[1]
            .insertAdjacentHTML(
              "beforeend",
              `<label for="pressure" class="cards-modal__pressure_label"
            >Обычное давление:</label
          >
          <input
            required
            type="text"
            class="cards-modal__pressure"
            id="pressure"
          />
          <label for="weight-index" class="cards-modal__weight-index_label"
            >Индекс массы тела:</label
          >
          <input
            required
            type="text"
            class="cards-modal__weight-index"
            id="weight-index"
          />
          <label for="diseases" class="cards-modal__diseases_label"
            >Перенесенные заболевания сердечно-сосудистой системы:</label
          >
          <input
            required
            type="text"
            class="cards-modal__diseases"
            id="diseases"
          />
          <label for="age" class="cards-modal__age_label">Возвраст:</label>
          <input required type="text" class="cards-modal__age" id="age" />`
            );
        } else if (this.value === "dentist") {
          document
            .querySelectorAll(".cards-modal__section")[1]
            .insertAdjacentHTML(
              "beforeend",
              `<label for="last-visit" class="cards-modal__last-visit_label">Дата последнего посещения:</label>
            <input required type="text" class="cards-modal__last-visit" id="last-visit" />`
            );
        } else {
          document
            .querySelectorAll(".cards-modal__section")[1]
            .insertAdjacentHTML(
              "beforeend",
              `<label for="age" class="cards-modal__age_label">Возвраст:</label>
              <input required type="text" class="cards-modal__age" id="age" />`
            );
        }
      });

    document
      .querySelector(".cards-modal__form")
      .addEventListener("submit", (e) => {
        e.preventDefault();

        const form = document.querySelector(".cards-modal__form");
        const formArr = Array.from(form).map((el) => el.value);
        const token = localStorage.getItem("token");

        if (formArr[4] === "cardiologist") {
          const [
            name,
            visitTarget,
            desc,
            urgency,
            doctor,
            pressure,
            bms,
            pastIllnesses,
            age,
          ] = formArr;

          postCardCardiolist(
            token,
            name,
            doctor,
            visitTarget,
            urgency,
            desc,
            pressure,
            bms,
            pastIllnesses,
            age,
            "POST",
            ""
          );

          getVisitInfo(token);

          document.querySelector(".body-page").classList.remove("blur");
          document.querySelector(".cards-modal").remove();
        } else if (formArr[4] === "dentist") {
          const [name, visitTarget, desc, urgency, doctor, lastVisit] = formArr;

          postCardDentist(
            token,
            name,
            doctor,
            visitTarget,
            urgency,
            desc,
            lastVisit,
            "POST",
            ""
          );
          getVisitInfo(token);
          document.querySelector(".body-page").classList.remove("blur");
          document.querySelector(".cards-modal").remove();
        } else if (formArr[4] === "therapist") {
          const [name, visitTarget, desc, urgency, doctor, age] = formArr;

          postCardTherapist(
            token,
            name,
            doctor,
            visitTarget,
            urgency,
            desc,
            age,
            "POST",
            ""
          );
          getVisitInfo(token);
          document.querySelector(".body-page").classList.remove("blur");
          document.querySelector(".cards-modal").remove();
        }
      });

    document
      .querySelector(".cards-modal__btn_cancel")
      .addEventListener("click", () => {
        document.querySelector(".body-page").classList.remove("blur");
        document.querySelector(".cards-modal").remove();
      });
  }
}

export default cardCreate;
